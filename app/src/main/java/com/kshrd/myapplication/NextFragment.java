package com.kshrd.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class NextFragment extends Fragment {

    public ArrayList<String> arrayList = new ArrayList<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        for (int i = 0; i < 30; i++) {
            arrayList.add("mail" + i + "@gmail.com");
        }

        View view = inflater.inflate(R.layout.fragment_next, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.mRecyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        CustomAdapter adapter = new CustomAdapter(arrayList, view.getContext());
        recyclerView.setAdapter(adapter);
        return view;
    }
}